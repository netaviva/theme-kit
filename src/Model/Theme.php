<?php

namespace PageWeb\ThemeKit\Model;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class Theme
{
    protected $name;

    protected $compiler;

    public function __construct(array $params)
    {
        $this->name = $params['name'];
        $this->compiler = $params['compiler'];
    }

    /**
     * @param mixed $compiler
     */
    public function setCompiler($compiler)
    {
        $this->compiler = $compiler;
    }

    /**
     * @return mixed
     */
    public function getCompiler()
    {
        return $this->compiler;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
