<?php

namespace PageWeb\ThemeKit;

use Faker\Factory;
use Faker\Generator;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class SiteProvider
{
    public $site = [
        'name' => 'netaviva',
        'title' => 'Netaviva',
        'subdomain' => 'netaviva',
        'email' => 'build@netaviva.com',
        'address' => 'No. 5 Herbert Macaulay'
    ];

    protected $routes = [
        'home' => '/',
        'gallery' => '/gallery'
    ];

    /**
     * @var \Faker\Generator
     */
    protected $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    public function getTitle()
    {
        return $this->site['title'];
    }

    public function getSubdomain()
    {
        return $this->site['subdomain'];
    }

    public function getPhone()
    {
        return $this->site['phone'];
    }

    public function getAddress()
    {
        return $this->site['address'];
    }

    public function getEmail()
    {
        return $this->site['email'];
    }

    public function albums()
    {

    }

    public function photos()
    {

    }

    public function posts()
    {
        $posts = [];
        for ($i = 0; $i < 20; $i++) {
            $posts[] = [
                'title' => implode(' ', $this->faker->words(3))
            ];
        }

        return $posts;
    }

    public function events()
    {

    }

    public function urlFor($path)
    {
        return '/' . $path;
    }

    public function urlRoute($route, $params = array())
    {
        return $this->routes[$route];
    }
}
