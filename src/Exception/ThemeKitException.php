<?php

namespace PageWeb\ThemeKit\Exception;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class ThemeKitException extends \Exception
{
}
