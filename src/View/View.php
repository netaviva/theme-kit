<?php

namespace PageWeb\ThemeKit\View;

use PageWeb\ThemeKit\Model\Theme;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class View
{
    public function __construct(CompilerStore $compilerStore, Theme $theme)
    {
        $this->theme = $theme;
        $this->compilerStore = $compilerStore;
    }

    public function render($view, $data = array())
    {
        $compiler = $this->compilerStore->get($this->theme->getCompiler());
        $compiler->setUp('./theme/views');
        $view = $view . $compiler->getExtension();

        return $compiler->render($view, $data);
    }
}
