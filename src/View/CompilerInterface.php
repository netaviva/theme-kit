<?php

namespace PageWeb\ThemeKit\View;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
interface CompilerInterface
{
    public function getName();

    public function getExtension();

    public function setUp($paths);

    public function render($file, $data = array());
}
