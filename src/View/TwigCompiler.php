<?php

namespace PageWeb\ThemeKit\View;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class TwigCompiler implements CompilerInterface
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    public function getName()
    {
        return 'twig';
    }

    public function getExtension()
    {
        return '.twig';
    }

    public function setUp($paths)
    {
        $loader = new \Twig_Loader_Filesystem($paths);
        $this->twig = new \Twig_Environment($loader);
    }

    public function render($view, $data = array())
    {
        return $this->twig->render($view, $data);
    }
}
