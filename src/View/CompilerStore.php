<?php

namespace PageWeb\ThemeKit\View;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class CompilerStore
{
    /**
     * @var CompilerInterface[]
     */
    protected $compilers = [];

    public function __construct()
    {
        $this->registerCompiler(new TwigCompiler());
    }

    public function registerCompiler(CompilerInterface $compiler)
    {
        $this->compilers[$compiler->getName()] = $compiler;
    }

    /**
     * @param $name
     * @return CompilerInterface
     */
    public function get($name)
    {
        $compiler = $this->compilers['twig'];
        if (isset($this->compilers[$name])) {
            $compiler = $this->compilers[$name];
        };

        return $compiler;
    }
}
 