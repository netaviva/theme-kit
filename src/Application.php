<?php

namespace PageWeb\ThemeKit;

use PageWeb\ThemeKit\Command\CreateThemeCommand;
use PageWeb\ThemeKit\Command\ServerCommand;
use PageWeb\ThemeKit\Command\WorkspaceCommand;
use Symfony\Component\Console\Application as ConsoleApplication;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class Application extends ConsoleApplication
{
    const VERSION = '0.1.0';

    public function __construct()
    {
        parent::__construct('PageWeb ThemeKit', self::VERSION);

        $this->add(new CreateThemeCommand());
        $this->add(new ServerCommand());
        $this->add(new WorkspaceCommand());
    }
}
