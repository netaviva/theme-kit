<?php

namespace PageWeb\ThemeKit\Command;

use PageWeb\ThemeKit\Operations;
use PageWeb\ThemeKit\View\CompilerStore;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class WorkspaceCommand extends Command
{
    public function __construct()
    {
        parent::__construct();

        $this->operations = new Operations(new Filesystem(), new CompilerStore());
    }

    /**
     * {@inheritDoc}
     */
    public function configure()
    {
        $this->setName('theme:workspace');
        $this->setDescription('Sets workspace directory.');

        $this->setHelp(<<<EOF
Sets up your workspace where your themes will be stored.
EOF
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dialog = $this->getHelper('dialog');

        $data['author'] = [
            'name' => $dialog->ask($output, 'Author Name: '),
            'email' => $dialog->ask($output, 'Author Email: '),
            'website' => $dialog->ask($output, 'Author Website: ')
        ];

        $this->operations->setWorkspace($data);
    }
}
