<?php

namespace PageWeb\ThemeKit\Command;

use PageWeb\ThemeKit\Exception\ThemeKitException;
use PageWeb\ThemeKit\Operations;
use PageWeb\ThemeKit\View\CompilerStore;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class CreateThemeCommand extends Command
{
    public function __construct()
    {
        parent::__construct();

        $this->operations = new Operations(new Filesystem(), new CompilerStore());
    }

    /**
     * {@inheritDoc}
     */
    public function configure()
    {
        $this->setName('theme:create');
        $this->setDescription('Creates a pageweb.co theme.');
        $this->addArgument('name', InputArgument::REQUIRED, 'Theme name');
        $this->addOption('server', InputOption::VALUE_NONE, null, 'Start server after creating theme');
        $this->setHelp(<<<EOF
Creates the structure and components required to start off building
a theme for pageweb.co
EOF
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $compiler = $this->getHelper('dialog')->ask(
            $output,
            'What template compiler do you want to use for your theme?' . "\n" .
            'Supported compilers:' . "\n" .
            '0: twig'
        );

        $this->operations->createTheme([
            'name' => $input->getArgument('name'),
            'compiler' => $compiler
        ]);

        if ($input->getOption('server')) {
            $serverCommand = $this->getApplication()->find('theme:server');
            $serverInput = new ArrayInput([
                'theme' => $input->getArgument('name')
            ]);

            $serverCommand->run($serverInput, $output);
        }
    }
}
