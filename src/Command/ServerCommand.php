<?php

namespace PageWeb\ThemeKit\Command;

use PageWeb\ThemeKit\Operations;
use PageWeb\ThemeKit\View\CompilerStore;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class ServerCommand extends Command
{
    public function __construct()
    {
        parent::__construct();

        $this->operations = new Operations(new Filesystem(), new CompilerStore());
    }

    /**
     * {@inheritDoc}
     */
    public function configure()
    {
        $this->setName('theme:server');
        $this->setDescription('Launches server to preview site.');
        $this->addArgument('theme', null, InputArgument::REQUIRED, 'Sets the theme to launch server.', './');
        $this->addOption('port', null, InputOption::VALUE_REQUIRED, 'Set the server port.', 3000);

        $this->setHelp(<<<EOF
Launches a server using php inbuilt web server to allow
you preview your themes in the browser
EOF
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ . '/../');
        $port = (int) $input->getOption('port');
        $theme = $input->getArgument('theme');
        $output->writeln('Server started at port [' . $port . ']; Press Ctrl+C to exit');
        exec('php -S localhost:' . $port . ' -t ./' . $theme);
    }
}
