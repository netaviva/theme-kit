<?php

namespace PageWeb\ThemeKit;

use PageWeb\ThemeKit\Exception\ThemeKitException;
use PageWeb\ThemeKit\Model\Theme;
use PageWeb\ThemeKit\Model\Workspace;
use PageWeb\ThemeKit\View\CompilerStore;
use PageWeb\ThemeKit\View\View;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

/**
 * @author Laju Morrison <morrelinko@gmail.com>
 */
class Operations
{
    protected $scriptDir;

    protected $workingSpaceDir;

    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $filesystem;

    protected $author;

    /**
     * Constructor
     *
     * @param Filesystem $filesystem
     * @param \PageWeb\ThemeKit\View\CompilerStore $compilerStore
     */
    public function __construct(Filesystem $filesystem, CompilerStore $compilerStore)
    {
        $this->filesystem = $filesystem;
        $this->scriptDir = __DIR__ . '/..';
        $this->workingSpaceDir = './';
        $this->compilerStore = $compilerStore;
    }

    /**
     * Creates a new theme
     *
     * @param array $data
     * <p>
     *  name:
     *  compiler:
     * </p>
     * @throws Exception\ThemeKitException
     */
    public function createTheme(array $data)
    {
        $dir = $this->workingSpaceDir . '/' . $data['name'];
        if ($this->filesystem->exists($dir)) {
            throw new ThemeKitException('Theme [' . $data['name'] . '] already exists.');
        }

        $this->filesystem->mirror($this->scriptDir . '/stub', $dir);
        file_put_contents(
            $dir . '/index.php',
            str_replace(
                ['{@scriptDir}'],
                [$this->scriptDir],
                file_get_contents($dir . '/index.php')
            )
        );

        $config = array_merge([
            'title' => ucwords($data['name']),
            'description' => '',
            'author' => $this->getAuthor(),
        ], $data);

        $compiler = $this->compilerStore->get($config['compiler']);
        $views = array_map(function ($item) use ($config, $compiler) {
            return './' . $config['name'] . '/theme/views/' . $item . $compiler->getExtension();
        }, ['index', 'events', 'event', 'gallery', 'photos', 'news', 'contact']);
        $this->filesystem->touch($views);

        file_put_contents($dir . '/config.yaml', Yaml::dump($config));
    }

    /**
     * Sets workspace for developing theme
     */
    public function setWorkspace(array $data)
    {
        touch($this->workingSpaceDir . '/themekit');

        $this->setAuthor($data['author']);
    }

    /**
     * Sets author details that will be attached to the theme
     * created by this user
     *
     * @param array $data
     */
    public function setAuthor(array $data)
    {
        $this->setOption('author', array_merge([
            'name' => null,
            'email' => null,
            'website' => null
        ], $data));
    }

    public function getAuthor()
    {
        return $this->getOption('author');
    }

    public function getOption($option)
    {
        $options = $this->getOptions();

        return array_key_exists($option, $options) ? $options[$option] : null;
    }

    /**
     * @param string $option
     * @param mixed $value
     */
    public function setOption($option, $value)
    {
        $this->ensureWorkingSpace();

        $options = $this->merge([
            'author' => [
                'name' => null,
                'email' => null,
                'website' => null
            ]
        ], $this->getOptions());

        $options[$option] = $value;

        file_put_contents($this->workingSpaceDir . '/themekit', json_encode($options));
    }

    public function getOptions()
    {
        $this->ensureWorkingSpace();

        $options = json_decode(file_get_contents($this->workingSpaceDir . '/themekit'), true);

        return is_array($options) ? $options : array();
    }

    public function merge()
    {
        $array = array();
        $args = func_get_args();

        // number of arrays we are merging
        $cnt = count($args);

        // Reverse the array so the first array will be popped and attached to the new array
        $args = array_reverse($args);

        while ($cnt) {
            $pop = array_pop($args);

            foreach ($pop as $key => $value) {
                if (array_key_exists($key, $array)) {
                    if (is_int($key)) {
                        $array[] = $value;
                    } else {
                        if (is_array($value) && is_array($array[$key])) {
                            $array[$key] = static::merge($array[$key], $value);
                        } else {
                            $array[$key] = $value;
                        }
                    }
                } else {
                    $array[$key] = $value;
                }
            }

            $cnt--;
        }

        return $array;
    }

    /**
     * @throws Exception\ThemeKitException
     */
    protected function ensureWorkingSpace()
    {
        if (!file_exists($this->workingSpaceDir . '/themekit')) {
            throw new ThemeKitException('Your working space has not been set.');
        }
    }
}
