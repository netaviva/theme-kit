<?php

$scriptDir = '{@scriptDir}';
require $scriptDir . '/vendor/autoload.php';

use PageWeb\ThemeKit\Model\Theme;
use PageWeb\ThemeKit\SiteProvider;
use Symfony\Component\Yaml\Yaml;

$uri = $_SERVER['REQUEST_URI'];
$site = new SiteProvider(Faker\Factory::create());
$view = new \PageWeb\ThemeKit\View\View(
    new \PageWeb\ThemeKit\View\CompilerStore(),
    new Theme(Yaml::parse('./config.yaml'))
);

foreach ([
    '/events/(?P<event_id>[0-9]+)' => function ($eventId) use ($site, $view) {
            return $view->render('event');
        },
    '/events' => function () use ($site, $view) {
            return $view->render('events', ['site' => $site]);
        },
    '/gallery/([0-9]+)' => function () use ($site, $view) {
            return $view->render('photos', ['site' => $site]);
        },
    '/gallery' => function () use ($site, $view) {
            return $view->render('gallery', ['site' => $site]);
        },
    '/news' => function () use ($site, $view) {
            return $view->render('news', ['site' => $site]);
        },
    '/' => function () use ($site, $view) {
            return $view->render('index', ['site' => $site]);
        }
] as $route => $callback) {
    if (preg_match('#^' . $route . '/?$#', $uri, $matches)) {
        foreach ($matches as $key => $match) {
            if (is_numeric($key)) {
                unset($matches[$key]);
            }
        }

        echo call_user_func_array($callback, $matches);
        break;
    }
}
