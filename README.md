PageWeb Dev Kit
======================

### Installation

    $ composer global require "pageweb/theme-kit=0.*"

Go to a directory you want to use as your workspace and execute

    $ pageweb theme:workspace

You will be asked to insert some details like your name and email.


### Creating a theme

    $ pageweb theme:create --name my_theme_name

A directory 'my_theme_name' will be created which contains a basic stub setup for your theme...

### Developing themes

An example theme

    <div id="header">
        <h2 id="logo">
            {{ $site->name }}
        </h2>
    </div>

The $site variable holds all information you can get about a site including its posts, feeds, events, albums and photos

Using the theme-kit, these site components are populated with sample data when they are viewed on your local machine...

### Testing your theme

The theme kit also allows you to preview how your theme will look like as if it were on pageweb (but with sample data)

To preview your theme, execute this command inside of your theme workspace

    $ pageweb theme:server --theme my_theme_name --port 3000

you can now visit http://localhost:3000 on your browser to preview your theme